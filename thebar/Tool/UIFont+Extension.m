//
//  UIFont+Extension.m
//  thebar
//
//  Created by FC on 17/4/12.
//  Copyright © 2017年 FC. All rights reserved.
//
#import "UIFont+Extension.h"

@implementation UIFont (Extension)
+ (UIFont *)fontWithDevice:(CGFloat)fontSize {
    if (SW > 375) {
        fontSize = fontSize + 3;
    }else if (SW == 375){
        fontSize = fontSize + 1.5;
    }else if (SW == 320){
        fontSize = fontSize;
    }
    UIFont *font = [UIFont systemFontOfSize:fontSize];
    return font;
}

/**
 *  专门为客户性别，年龄电话写的
 */
+ (UIFont *)fontWithCustomer:(CGFloat)fontSize {
    if (SW > 375) {
        fontSize = fontSize + 2;
    }else if (SW == 375){
        fontSize = fontSize + 1.5;
    }else if (SW == 320){
        fontSize = fontSize;
    }
    UIFont *font = [UIFont systemFontOfSize:fontSize];
    return font;
}

+ (UIFont *)navItemFontWithDevice:(CGFloat)fontSize {
    if (SW > 375) {
        fontSize = fontSize + 2;
    }else if (SW == 375){
        fontSize = fontSize + 1;
    }else if (SW == 320){
        fontSize = fontSize;
    }
    UIFont *font = [UIFont systemFontOfSize:fontSize];
    return font;
}

+ (UIFont *)fontWithTwoLine:(CGFloat)fontSize {
    if (SW > 375) {
        fontSize = fontSize + 2;
    }else if (SW == 375){
        fontSize = fontSize + 1;
    }else if (SW == 320){
        fontSize = fontSize;
    }
    UIFont *font = [UIFont systemFontOfSize:fontSize];
    return font;
}

+ (UIFont *)insuranceCellFont:(CGFloat)fontSize {
    if (SW > 375) {
        fontSize = fontSize + 3.5;
    }else if (SW == 375){
        fontSize = fontSize + 2;
    }else if (SW == 320){
        fontSize = fontSize;
    }
    UIFont *font = [UIFont systemFontOfSize:fontSize];
    return font;
}

@end
