//
//  RequestTool.h
//  thebar
//
//  Created by FC on 17/4/12.
//  Copyright © 2017年 FC. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface RequestTool : NSObject

+ (RequestTool *)defaultManager;

//POST
/**
 *  请求数据
 *
 *  @param function     接口字段
 *  @param parameters   参数描述
 *  @param finsshed 完成Block
 */
- (void)postWithFunction:(NSString *)function
                 parameters:(NSDictionary *)parameters
                finished:(void(^)(id responseObject))SucccessBlock error:(void(^)(NSError *error))ErrorBlock;
//Get
-(void)getWithFunction:(NSString *)function finished:(void(^)(id responseObject))SucccessBlock error:(void(^)(NSError *error))ErrorBlock;


/**
 *  上传图片
 *
 *  @param function     接口字段
 *  @param parameters   参数描述
 *  @param dataArray    图片数据数组
 *  @param names        图片名数据数组
 *  @param fileNames    名称数据数组
 *  @param mimeType     类型数据数组
 *  @param finsh 完成Block
 */
- (void)uploadImageWithFunction:(NSString *)function
                     parameters:(NSDictionary *)parameters
                      dataArray:(NSArray <NSData *> *)dataArray
                          names:(NSArray <NSString *> *)names
                      fileNames:(NSArray <NSString *> *)fileNames
                       mimeType:(NSString *)mimeType
                       finished:(void(^)(id responseObject))SucccessBlock
                          error:(void(^)(NSError *error))ErrorBlock;



@end
