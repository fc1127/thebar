//
//  RequestTool.m
//  thebar
//
//  Created by FC on 17/4/12.
//  Copyright © 2017年 FC. All rights reserved.
//

#import "RequestTool.h"
#import "AFNetworking.h"

@implementation RequestTool


#pragma mark - Singleton
+ (RequestTool *)defaultManager
{
    static RequestTool *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [RequestTool new];
    });
    return manager;
}
-(void)getWithFunction:(NSString *)function finished:(void (^)(id))SucccessBlock error:(void (^)(NSError *))ErrorBlock{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    // 接收类型设置为 text/html
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    // 发起Post请求
    [manager.requestSerializer setValue:@"3.0" forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Type"];
    [manager GET:@"" parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        SucccessBlock(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        ErrorBlock(error);
    }];
    
}


-(void)postWithFunction:(NSString *)function parameters:(NSDictionary *)parameters finished:(void (^)(id))SucccessBlock error:(void (^)(NSError *))ErrorBlock{

    // 拼接已有的参数
    NSMutableDictionary *parameter = [NSMutableDictionary dictionaryWithDictionary:parameters];
//    parameter[@"api_token"] = [[Utils defaultUtils] getApiTokenWithFunction:function];
    // 通过网址和接口功能字符串拼接成接口网址
    NSString *urlString = [NSString stringWithFormat:@"%@%@", @"", function];
    // SessionManager
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    // 接收类型设置为 text/html
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    // 发起Post请求
    [manager.requestSerializer setValue:@"3.0" forHTTPHeaderField:@"Version"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"Type"];
    [manager POST:urlString parameters:parameter progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        SucccessBlock(responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        ErrorBlock(error);
        NSLog(@"%@", error.localizedDescription);
    }];
    
}


#pragma mark - 上传图片
/**
 *  上传图片
 *
 *  @param function     接口字段
 *  @param parameters   参数描述
 *  @param dataArray    图片数据数组
 *  @param names        图片名数据数组
 *  @param fileNames    名称数据数组
 *  @param mimeType     类型
 *  @param requestBlock 完成Block
 */

- (void)uploadImageWithFunction:(NSString *)function
                     parameters:(NSDictionary *)parameters
                      dataArray:(NSArray <NSData *> *)dataArray
                          names:(NSArray <NSString *> *)names
                      fileNames:(NSArray <NSString *> *)fileNames
                       mimeType:(NSString *)mimeType
                       finished:(void (^)(id))SucccessBlock
                       error:(void (^)(NSError *))ErrorBlock{
    if (!(dataArray.count == names.count == fileNames.count))
        return;
    // show hud
//    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient];
    // 拼接已有的参数
    NSMutableDictionary *parameter = [NSMutableDictionary dictionaryWithDictionary:parameters];
//    parameter[@"api_token"] = [[Utils defaultUtils] getApiTokenWithFunction:function];
    // 通过网址和接口功能字符串拼接成接口网址
    NSString *urlString = [NSString stringWithFormat:@"%@%@", @"", function];
    // SessionManager
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    // PostWithFileData
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [manager POST:urlString parameters:parameter constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        for (int i = 0; i < dataArray.count; i++) {
            [formData appendPartWithFileData:dataArray[i] name:names[i] fileName:fileNames[i] mimeType:mimeType];
            
        }
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        SucccessBlock(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        ErrorBlock(error);
        NSLog(@"%@", error.localizedDescription);
    }];
}





@end
