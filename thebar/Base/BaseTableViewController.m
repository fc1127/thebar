//
//  BaseTableViewController.m
//  HKSpace
//
//  Created by FC on 17/4/9.
//  Copyright © 2017年 FC. All rights reserved.
//

#import "BaseTableViewController.h"


@interface BaseTableViewController ()



@end

@implementation BaseTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view addSubview:self.tableview];


}

#pragma mark UITableViewDataSource/UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
       return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
        return 20;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:@"cellid"];
    if (cell== nil){
    
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellid"];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.textLabel.text=[NSString stringWithFormat:@"%ld",indexPath.row];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 44;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self.view endEditing:YES];
}

#pragma mark - refreshData

/**
 *  刷新tableView
 */
- (void)refreshData {
    self.dataSource = nil;
    [self.tableview reloadData];
}
#pragma mark - MJRefresh上下拉加载

- (void)refresh {
    self.pageIndex = 1;
    [self requestData];
    [self.tableview.mj_header endRefreshing];
}

- (void)loadMore {
    [self requestData];
    [self.tableview.mj_footer endRefreshing];
}
-(void)requestData{

}




//- (void)removedRefreshing {
//    _header = nil;
//    _footer = nil;
//    self.tableview.mj_header= nil;
//    self.tableview.mj_footer = nil;
//}

//- (void)showLoadMoreRefreshing {
//    self.footer.hidden = NO;
//}
//
//- (void)hideLoadMoreRefreshing {
//    self.footer.hidden = YES;
//}



#pragma mark - 懒加载

- (UITableView *)tableview {
    if (!_tableview) {
        _tableview = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SW, self.view.frame.size.height) style:UITableViewStylePlain];
        _tableview.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        _tableview.dataSource = self;
        _tableview.delegate = self;
        _tableview.backgroundColor = [UIColor clearColor];
        _tableview.tableFooterView=[[UIView alloc]init];
//        _tableview.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        self.tableview.mj_header= [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(refresh)];
          self.tableview.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMore)];
    }
    return _tableview;
}



@end
