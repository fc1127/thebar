//
//  BaseViewController.h
//  thebar
//
//  Created by FC on 17/4/12.
//  Copyright © 2017年 FC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController

@property(nonatomic,assign) NSInteger pageIndex;//当前页

#pragma mark 公用方法
- (void)requestData;//网络请求
//- (void)backAction:(UIButton *)sender;//返回
- (void)gotoLoginViewController;//去登陆界面


@end


