//
//  BaseTableViewController.h
//  HKSpace
//
//  Created by FC on 17/4/9.
//  Copyright © 2017年 FC. All rights reserved.
//

#import "BaseViewController.h"
#import "MJRefresh.h"
#import "MBProgressHUD.h"

@interface BaseTableViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate>


@property (nonatomic,strong) UITableView *tableview;
@property (nonatomic,strong) NSArray *dataSource;//cell相关data
@property (nonatomic,strong) MJRefreshNormalHeader *header;
@property (nonatomic,strong) MJRefreshAutoNormalFooter *footer;

- (NSArray *)cellDataSource;


#pragma mark - 上下拉加载
- (void)removedRefreshing;//去掉上下拉刷新 列表默认添加 如不需要可调用该方法移除
- (void)refresh;//下拉请求
- (void)loadMore;//加载更多
- (void)endRefreshing;//结束刷新
- (void)hideLoadMoreRefreshing;//隐藏加载更多
- (void)showLoadMoreRefreshing;//显示加载更多



//刷新数据
- (void)refreshData;


@end
