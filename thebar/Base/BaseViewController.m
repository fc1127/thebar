//
//  BaseViewController.m
//  thebar
//
//  Created by FC on 17/4/12.
//  Copyright © 2017年 FC. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    
    self.view.backgroundColor = UIColorFromRGB(kMainBg);
    
    
#ifdef __IPHONE_7_0
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    if (self.navigationController)
    {
        self.view.frame = kcr(0, 0, SW, SH - TOP_HEIGHT);
    }
#endif
}
-(void)requestData{}
- (void)gotoLoginViewController {}

@end
