//
//  FCTabBar.h
//  thebar
//
//  Created by FC on 17/4/11.
//  Copyright © 2017年 FC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FCTabBar;
@protocol FCTabBarDelegate <NSObject>

@optional
- (void)tabBar:(FCTabBar *)tabBar didClickBtn:(NSInteger)index;

@end

@interface FCTabBar : UIView
/** 选中的索引 */
@property (nonatomic, assign) NSInteger seletedIndex;

// 模型数组(UITabBarItem)
@property (nonatomic, strong) NSArray *items;

@property (nonatomic, weak) id<FCTabBarDelegate> delegate;



@end
