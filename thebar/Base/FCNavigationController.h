//
//  FCNavigationController.h
//  thebar
//
//  Created by FC on 17/4/11.
//  Copyright © 2017年 FC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FCNavigationController : UINavigationController

@property (nonatomic, strong) id popDelegate;

@end
