//
//  AppDelegate.h
//  thebar
//
//  Created by FC on 17/4/11.
//  Copyright © 2017年 FC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

